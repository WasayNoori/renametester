﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EdmLib;

namespace RenameTester
{
    class Program
    {
       
        static void Main(string[] args)
        {
            IEdmVault12 _vault;
            _vault = (IEdmVault12)new EdmVault5();
            _vault.LoginAuto("PDM_Vault",0);

            IEdmFolder5 parentFold = _vault.GetFolderFromPath(@"C:\Vaults\PDM_VAULT\ECO\PN SAMPLES");
            List<IEdmFile10> getFiles = filesinfolder(parentFold, _vault);
            string extn;
            int counter=2;
            foreach(IEdmFile10 file in getFiles)
            {
                try
                {
                    extn = System.IO.Path.GetExtension(file.Name);
                    file.RenameEx(0, counter.ToString() + extn, 0);
                    file.LockFile(parentFold.ID,(int) EdmLockFlag.EdmLock_Simple);
                    IEdmEnumeratorVariable9 enumVar;
                    enumVar = (IEdmEnumeratorVariable9)file.GetEnumeratorVariable();
                    string config = getFirstConfig(_vault, file.ID);
                    enumVar.SetVar("Number", config, counter.ToString() + " Variable");
                    enumVar.CloseFile(true);
                    file.UnlockFile(0, "Changed variable",(int) EdmUnlockFlag.EdmUnlock_IgnoreRefsNotLockedByCaller 
                        + (int)EdmUnlockFlag.EdmUnlock_IgnoreRefsOutsideVault);
                    
                }
                catch (Exception ex)
                {

                    Console.WriteLine("Error in file " + file.Name + " " + ex.Message);
                }

                counter++;  
            }

            Console.WriteLine("Finished.");
            Console.Read();
        }

        private static List<IEdmFile10>filesinfolder(IEdmFolder5 fold,IEdmVault12 vault)
        {
            List<IEdmFile10> myFiles = new List<IEdmFile10>();


            if(fold==null )
            {
                return myFiles;
            }

            IEdmPos5 pos;
            IEdmFile10 myFile;
            pos = fold.GetFirstFilePosition();
            while (!pos.IsNull)
            {
                myFile = (IEdmFile10)fold.GetNextFile(pos);
                if(myFile!=null)
                {
                    myFiles.Add(myFile);
                }
            }
            return myFiles;
        }

        private static string getFirstConfig(IEdmVault12 vault, int fileID)
        {
            //Returns the first configuratino of a file. For SOLIDWORKS, it is usually @.
            IEdmFile8 file;
            try
            {
                file = (IEdmFile8)vault.GetObject(EdmObjectType.EdmObject_File, fileID);
            }
            catch (Exception)
            {

                return "";
            }


            EdmStrLst5 cfgList = file.GetConfigurations();


            List<string> configList = new List<string>();
            IEdmPos5 pos;
            pos = (IEdmPos5)cfgList.GetHeadPosition();
            string cfgName;

            while (!pos.IsNull)
            {
                cfgName = cfgList.GetNext(pos);
                configList.Add(cfgName);
            }

            if (configList.Count > 0)
            {
                return configList[0];
            }
            else
            {
                return "";
            }



        }
    }
}
